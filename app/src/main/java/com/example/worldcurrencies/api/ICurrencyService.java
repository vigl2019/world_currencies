package com.example.worldcurrencies.api;

import com.example.worldcurrencies.model.CurrencyRates;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ICurrencyService {
    @GET("latest")
    Call<CurrencyRates> getRates(@Query("access_key") String access_key,
                                 @Query("base") String base,
                                 @Query("symbols") String symbols);

    @GET("{date}")
    Call<CurrencyRates> getRatesByDate(@Path("date") String date,
                                       @Query("access_key") String access_key,
                                       @Query("base") String base,
                                       @Query("symbols") String symbols);
}