package com.example.worldcurrencies.api;

import androidx.annotation.NonNull;

import com.example.worldcurrencies.BuildConfig;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ApiFactory {
    private static OkHttpClient client;
    private static volatile ICurrencyService CurrencyService;

    private ApiFactory() {
    }

    @NonNull
    public static ICurrencyService getCurrencyService() {
        ICurrencyService iCurrencyService = CurrencyService;

        if (iCurrencyService == null) {
            synchronized (ApiFactory.class) {
                iCurrencyService = CurrencyService;
                if (iCurrencyService == null) {
                    iCurrencyService = CurrencyService = buildRetrofit().create(ICurrencyService.class);
                }
            }
        }
        return iCurrencyService;
    }

    public static void recreate(){
        client = null;
        client = getClient();
        CurrencyService = buildRetrofit().create(ICurrencyService.class);
    }

    @NonNull
    private static Retrofit buildRetrofit() {

        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(getClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @NonNull
    private static OkHttpClient getClient() {
        OkHttpClient okHttpClient = client;

        if (okHttpClient == null) {
            synchronized (ApiFactory.class) {
                okHttpClient = client;

                if (okHttpClient == null) {
                    okHttpClient = client = buildClient();
                }
            }
        }
        return okHttpClient;
    }

    @NonNull
    private static OkHttpClient buildClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(LoggingInterceptor.create())
                .build();
    }
}