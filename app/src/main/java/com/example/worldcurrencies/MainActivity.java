package com.example.worldcurrencies;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.worldcurrencies.adapter.CurrencyAdapter;
import com.example.worldcurrencies.model.CurrencyModel;
import com.example.worldcurrencies.utils.Helper;
import com.example.worldcurrencies.utils.IHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.am_currency_list)
    RecyclerView recyclerViewCurrency;

    private Helper helper;
    private CurrencyAdapter currencyAdapter;

    public IHelper iHelper = new IHelper() {
        @Override
        public void getCurrencyModels(List<CurrencyModel> currencyModels) {
            recyclerViewInit(currencyModels);
        }
    };

    //================================================================================//

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        helper = new Helper(MainActivity.this);
        helper.getAllCurrencies(iHelper);
    }

    private void recyclerViewInit(List<CurrencyModel> currencyModels){

        currencyAdapter = new CurrencyAdapter(currencyModels, false);

        currencyAdapter.setItemClickListener(new CurrencyAdapter.OnItemClickListener() {
            @Override
            public void onClick(CurrencyModel currencyModel) {

                Intent intent = new Intent(MainActivity.this, CurrencyInfoActivity.class);
                intent.putExtra("CurrencyModel", currencyModel);
                startActivity(intent);
            }
        });

        currencyAdapter.setLongItemClickListener(new CurrencyAdapter.OnLongItemClickListener() {
            @Override
            public void onLongClick(CurrencyModel currencyModel) {

                Intent intent = new Intent(MainActivity.this, CurrencySearchActivity.class);
                intent.putExtra("CurrencyModel", currencyModel);
                startActivity(intent);
            }
        });

        recyclerViewCurrency.setAdapter(currencyAdapter);
    }
}