package com.example.worldcurrencies.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.worldcurrencies.R;
import com.example.worldcurrencies.model.CurrencyModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder> {

    private List<CurrencyModel> currencyModels;
    private boolean isShowRate;
    private OnItemClickListener itemClickListener;
    private OnLongItemClickListener longItemClickListener;

    public CurrencyAdapter(List<CurrencyModel> currencyModels, boolean isShowRate) {
        this.currencyModels = currencyModels;
        this.isShowRate = isShowRate;
    }

    //================================================================================//

    public interface OnItemClickListener {
        void onClick(CurrencyModel currencyModel);
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }


    public interface OnLongItemClickListener {
        void onLongClick(CurrencyModel currencyModel);
    }

    public void setLongItemClickListener(OnLongItemClickListener longItemClickListener) {
        this.longItemClickListener = longItemClickListener;
    }

    //================================================================================//

    @NonNull
    @Override
    public CurrencyAdapter.CurrencyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View currencyView = inflater.inflate(R.layout.currency_item, parent, false);
        CurrencyViewHolder currencyViewHolder = new CurrencyViewHolder(currencyView);

        return currencyViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CurrencyAdapter.CurrencyViewHolder holder, int position) {

        CurrencyModel currencyModel = currencyModels.get(position);

        holder.currencyIcon.setImageResource(currencyModel.getCurrencyIconId());
        holder.currencyCode.setText(currencyModel.getCurrencyCode());
        holder.currencyTitle.setText(currencyModel.getCurrencyTitle());

        if (isShowRate == true) {
            holder.currencyRate.setVisibility(View.VISIBLE);
            holder.currencyRate.setText(currencyModel.getCurrencyRate());
        } else {
            holder.currencyRate.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return currencyModels.size();
    }

    //================================================================================//

    class CurrencyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ci_currency_icon)
        ImageView currencyIcon;

        @BindView(R.id.ci_currency_code)
        TextView currencyCode;

        @BindView(R.id.ci_currency_title)
        TextView currencyTitle;

        @BindView(R.id.ci_currency_rate)
        TextView currencyRate;


        public CurrencyViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(itemClickListener != null)
                        itemClickListener.onClick(currencyModels.get(getAdapterPosition()));
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (longItemClickListener != null) {
                        longItemClickListener.onLongClick(currencyModels.get(getAdapterPosition()));
                        return true;
                    }
                    return false;
                }
            });
        }
    }
}