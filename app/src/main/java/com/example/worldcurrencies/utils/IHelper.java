package com.example.worldcurrencies.utils;

import com.example.worldcurrencies.model.CurrencyModel;

import java.util.List;

public interface IHelper {
    void getCurrencyModels(List<CurrencyModel> currencyModels);
}