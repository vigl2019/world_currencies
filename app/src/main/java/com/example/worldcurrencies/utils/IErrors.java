package com.example.worldcurrencies.utils;

import java.util.HashMap;
import java.util.Map;

public interface IErrors {

    enum Errors {
        YEAR_FOUR_DIGITS("Year must have four digits"),
        MONTH_TWO_DIGITS("Month must have two digits"),
        YEAR_IS_MORE_THAN_CURRENT("Year is more than current"),
        MONTH_IS_MORE_THAN_CURRENT("Month is more than current"),
        DAY_IS_MORE_THAN_CURRENT("Day of the month is more than current"),
        DAY_OF_MONTH_ERROR("Day of the month less than 1 or more than 31"),
        MONTH_ERROR("Month less than 1 or more than 12"),
        YEAR_ERROR("Year less than 1"),
        FEBRUARY_ERROR("Day of February more than 29"),
        LEAP_YEAR_ERROR("Don't a leap year. Number of days of February must be 28"),
        DAY_TWO_DIGITS("Day of the month must have two digits"),
        EMPTY_TEXT_FIELD("Enter date");

        static Map<Integer, String> errorsMap = new HashMap<>();

        static {

            errorsMap.put(1, YEAR_FOUR_DIGITS.getErrorMassage());
            errorsMap.put(2, MONTH_TWO_DIGITS.getErrorMassage());
            errorsMap.put(3, YEAR_IS_MORE_THAN_CURRENT.getErrorMassage());
            errorsMap.put(4, MONTH_IS_MORE_THAN_CURRENT.getErrorMassage());
            errorsMap.put(5, DAY_IS_MORE_THAN_CURRENT.getErrorMassage());
            errorsMap.put(6, DAY_OF_MONTH_ERROR.getErrorMassage());
            errorsMap.put(7, MONTH_ERROR.getErrorMassage());
            errorsMap.put(8, YEAR_ERROR.getErrorMassage());
            errorsMap.put(9, FEBRUARY_ERROR.getErrorMassage());
            errorsMap.put(10, LEAP_YEAR_ERROR.getErrorMassage());
            errorsMap.put(12, DAY_TWO_DIGITS.getErrorMassage());
            errorsMap.put(15, EMPTY_TEXT_FIELD.getErrorMassage());
        }

        private String errorMassage;

        Errors(String errorMassage) {
            this.errorMassage = errorMassage;
        }

        public String getErrorMassage() {
            return errorMassage;
        }

        public void setErrorMassage(String errorMassage) {
            this.errorMassage = errorMassage;
        }

        public static String getErrorString(int errorCode) {
            return errorsMap.get(errorCode);
        }
    }
}