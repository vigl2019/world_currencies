package com.example.worldcurrencies.utils;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Validator {

    public static String getErrorText(int errorCode) {
        return IErrors.Errors.getErrorString(errorCode);
    }

    public static int checkTextView(String text){
        if (text.trim().isEmpty())
            return 15;
        return 0;
    }

    //================================================================================//

    public static int checkSearchDate(String searchDateString) {

        String[] searchDate = Helper.getStringDateArrayForSearch(searchDateString);

        if(searchDate.length < 3){
            return 15;
        }

        //==================================================//

        if(searchDate[0].length() == 0 || searchDate[0].length() < 4){
            return 1;
        }
        if(searchDate[1].length() == 0){
            return 2;
        }
        if(searchDate[2].length() == 0){
            return 12;
        }

        //==================================================//

        int year = Integer.parseInt(searchDate[0]);
        int month = Integer.parseInt(searchDate[1]) - 1;
        int dayOfMonth = Integer.parseInt(searchDate[2]);

        //==================================================//

        GregorianCalendar calendar = new GregorianCalendar();

        int currentDayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int currentMonth = calendar.get(Calendar.MONTH);
        int currentYear = calendar.get(Calendar.YEAR);

        if (year > currentYear)
            return 3;

        if (year == currentYear) {
            if (month > currentMonth)
                return 4;
        }

        if (month == currentMonth) {
            if (dayOfMonth > currentDayOfMonth)
                return 5;
        }

        //==================================================//

        if (dayOfMonth <= 0 || dayOfMonth > 31)
            return 6;

        if (month < 1 || month > 12)
            return 7;

        if (year < 1)
            return 8;

        // Check February
        if (month == 2) {
            if (dayOfMonth > 29)
                return 9;
            if (dayOfMonth == 29) {

                // Check leap year
                if (!calendar.isLeapYear(year))
                    return 10;
/*
                if (((year % 4) == 0 && (year % 100) != 0) || (year % 400) == 0) {
                    return 0;
                } else
                    return 10;
            }
*/

            }
        }
        return 0;
    }
}


