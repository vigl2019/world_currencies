package com.example.worldcurrencies.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.widget.Toast;

import com.example.worldcurrencies.BuildConfig;
import com.example.worldcurrencies.R;
import com.example.worldcurrencies.model.CurrencyModel;
import com.example.worldcurrencies.model.CurrencyRates;
import com.example.worldcurrencies.repository.CurrencyRepository;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Helper {

    private Context context;
    private String apiKey;
    private String baseCurrency;
    private CurrencyRepository currencyRepository;

    public Helper(Context context) {
        this.context = context;

        apiKey = BuildConfig.API_KEY;
        baseCurrency = BuildConfig.BASE_CURRENCY;
        currencyRepository = new CurrencyRepository();
    }

    //================================================================================//

    public void getAllCurrencies(final IHelper iHelper) {

//      int currencyDescriptionArrayId = context.getResources().getIdentifier("currenciesDescription", "array", context.getPackageName());

        String currenciesCode = "";
        final String[] currenciesCodeArray = context.getResources().getStringArray(R.array.currenciesCode);
        for (int i = 0; i < currenciesCodeArray.length; i++) {
            if (i != (currenciesCodeArray.length - 1)) {
                currenciesCode += currenciesCodeArray[i] + ",";
            } else {
                currenciesCode += currenciesCodeArray[i];
            }
        }

        getCurrenciesFromServer(context, iHelper, currenciesCode, currenciesCodeArray);
    }

    public void getCurrencies(final IHelper iHelper, CurrencyModel currencyModel) {

        String currenciesCode = Helper.getCurrenciesCode(context, currencyModel.getCurrencyCode());
        final String[] currenciesCodeArray = Helper.getCurrenciesCodeArray(context, currencyModel.getCurrencyCode());

        getCurrenciesFromServer(context, iHelper, currenciesCode, currenciesCodeArray);
    }

    public void getCurrenciesByDate(String searchDate, final IHelper iHelper, CurrencyModel currencyModel) {

        String currenciesCode = Helper.getCurrenciesCode(context, currencyModel.getCurrencyCode());
        final String[] currenciesCodeArray = Helper.getCurrenciesCodeArray(context, currencyModel.getCurrencyCode());

        getCurrenciesFromServerByDate(searchDate, context, iHelper, currenciesCode, currenciesCodeArray);
    }

    private void getCurrenciesFromServer(final Context context,
                                         final IHelper iHelper, String currenciesCode,
                                         final String[] currenciesCodeArray) {

        currencyRepository.getData(apiKey, baseCurrency, currenciesCode).enqueue(new Callback<CurrencyRates>() {

            @Override
            public void onResponse(Call<CurrencyRates> call, Response<CurrencyRates> response) {

                if (response.isSuccessful() && response.body() != null) {
                    List<CurrencyModel> currencyModels = Helper.fillCurrencyModelsList(context, currenciesCodeArray, response.body());
                    iHelper.getCurrencyModels(currencyModels);

                } else {
//                  Toast.makeText(context, context.getString(R.string.any_error), Toast.LENGTH_SHORT).show();
                    createAlertDialog(context.getString(R.string.any_error), context);
                }
            }

            @Override
            public void onFailure(Call<CurrencyRates> call, Throwable t) {
//              Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
//              createAlertDialog(t.getMessage(), context);
                createAlertDialog(context.getString(R.string.internet_error), context);
            }
        });
    }

    private void getCurrenciesFromServerByDate(String searchDate, final Context context,
                                               final IHelper iHelper, String currenciesCode,
                                               final String[] currenciesCodeArray) {
        currencyRepository.getDataByDate(searchDate, apiKey, baseCurrency, currenciesCode).enqueue(new Callback<CurrencyRates>() {

            @Override
            public void onResponse(Call<CurrencyRates> call, Response<CurrencyRates> response) {

                if (response.isSuccessful() && response.body() != null) {
                    List<CurrencyModel> currencyModels = Helper.fillCurrencyModelsList(context, currenciesCodeArray, response.body());
                    iHelper.getCurrencyModels(currencyModels);

                } else {
//                  Toast.makeText(context, context.getString(R.string.any_error), Toast.LENGTH_SHORT).show();
                    createAlertDialog(context.getString(R.string.any_error), context);
                }
            }

            @Override
            public void onFailure(Call<CurrencyRates> call, Throwable t) {
//              Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
//              createAlertDialog(t.getMessage(), context);
                createAlertDialog(context.getString(R.string.internet_error), context);
            }
        });
    }

    //================================================================================//

    public static List<CurrencyModel> fillCurrencyModelsList(Context context,
                                                             String[] currenciesCodeArray,
                                                             CurrencyRates currencyRates) {

        List<CurrencyModel> currencyModels = new ArrayList<>();

        for (int i = 0; i < currenciesCodeArray.length; i++) {

            CurrencyModel currencyModel = new CurrencyModel();

            String currencyTitle = Helper.getCurrencyInfo(context, currenciesCodeArray[i],
                                    context.getString(R.string.currencies_title_file_name));

            currencyModel.setCurrencyTitle(currencyTitle);
            currencyModel.setCurrencyCode(currenciesCodeArray[i]);
            currencyModel.setCurrencyIconName(currenciesCodeArray[i].trim().toLowerCase());
            currencyModel.setCurrencyIconIdByCurrencyCode(context);

            //==================================================//

            String currencyDescription = Helper.getCurrencyInfo(context, currenciesCodeArray[i],
                                            context.getString(R.string.currencies_description_file_name));
            currencyModel.setCurrencyDescription(currencyDescription);

            //==================================================//

            double currencyRate = 0;
            try {
                Method method = CurrencyRates.Rates.class.getDeclaredMethod("get" + currenciesCodeArray[i]);
                currencyRate = Double.parseDouble(String.valueOf(method.invoke(currencyRates.getRates())));
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }

            currencyModel.setCurrencyRate(Helper.convertNumberToString(currencyRate));

            currencyModels.add(currencyModel);

/*
                        for(int j = 0; j < methods.length; j++){

                            String methodName = methods[i].getName();

                            if(methodName.trim().toLowerCase().equals("get" + currenciesCodeArray[i])) {

                                try {
                                    Method method = CurrencyRates.Rates.class.getDeclaredMethod(methodName);
                                    Double currencyRate = (Double)method.invoke(currencyRates);
                                    currencyModel.setCurrencyRate(currencyRate);
                                }
                                catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e){
//                                  e.printStackTrace();
                                }

                                break;
                            }
                        }
*/
        }

        return currencyModels;
    }

    //================================================================================//

    public static String getAllCurrenciesCode(Context context) {

        String currenciesCode = "";
        String[] currenciesCodeArray = context.getResources().getStringArray(R.array.currenciesCode);

        for (int i = 0; i < currenciesCodeArray.length; i++) {
            if (i != (currenciesCodeArray.length - 1))
                currenciesCode += currenciesCodeArray[i] + ",";
            else
                currenciesCode += currenciesCodeArray[i];
        }

        return currenciesCode;
    }

    public static String getCurrenciesCode(Context context, String removeCurrency) {

        String currenciesCode = "";
        String[] currenciesCodeArray = getCurrenciesCodeArray(context, removeCurrency);

        for (int i = 0; i < currenciesCodeArray.length; i++) {
            if (i != (currenciesCodeArray.length - 1))
                currenciesCode += currenciesCodeArray[i] + ",";
            else
                currenciesCode += currenciesCodeArray[i];
        }

        return currenciesCode;
    }

    public static String[] getCurrenciesCodeArray(Context context, String removeCurrency) {

        String[] currenciesAllCodeArray = context.getResources().getStringArray(R.array.currenciesCode);
        String[] currenciesCodeArray = new String[currenciesAllCodeArray.length - 1];

        int j = 0;
        for (int i = 0; i < currenciesAllCodeArray.length; i++) {
            if (!currenciesAllCodeArray[i].equals(removeCurrency))
                currenciesCodeArray[j++] = currenciesAllCodeArray[i];
        }

        return currenciesCodeArray;
    }

    //================================================================================//

    public static String getCurrencyInfo(Context context, String currencyCode, String fileInfoName) {

        String currencyDescription = "";

        AssetManager assetManager = context.getAssets();
        InputStream inputStream;

        XmlPullParserFactory parserFactory;

        try {
            inputStream = assetManager.open(Helper.getPropertyValue(fileInfoName, context));

            parserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = parserFactory.newPullParser();

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(inputStream, null);

            currencyDescription = doParsing(parser, currencyCode);

        } catch (XmlPullParserException | IOException e) {
//          e.printStackTrace();
        }

        return currencyDescription;
    }

    private static String doParsing(XmlPullParser parser, String currencyCode)
                                    throws IOException, XmlPullParserException {

        String currencyDescription = "";
        int eventType = parser.getEventType();

        while (eventType != XmlPullParser.END_DOCUMENT) {
            String tagName;

            switch (eventType) {
                case XmlPullParser.START_TAG: {
                    tagName = parser.getName();

                    if (tagName.equals(currencyCode.trim().toLowerCase()))
                        currencyDescription = parser.nextText();

                    break;
                }
                default:
                    break;
            }
            eventType = parser.next();
        }

        return currencyDescription;
    }

    //================================================================================//

    public static boolean validateSearchDateField(String searchDateString, Context context) {

        int errorCode = Validator.checkTextView(searchDateString);
        if (errorCode > 0) {
            createAlertDialog(Validator.getErrorText(errorCode), context);
            return false;
        }

        errorCode = Validator.checkSearchDate(searchDateString);
        if (errorCode > 0) {
            createAlertDialog(Validator.getErrorText(errorCode), context);
            return false;
        }

        return true;
    }

    public static String[] getStringDateArrayForSearch(String searchDateString) {

        if (searchDateString.contains("--")) {
            searchDateString = searchDateString.replace("--", "-");
        }

        String[] searchDate = searchDateString.split("-");

        if (searchDate.length == 3) {
            if (searchDate[1].length() == 1)
                searchDate[1] = "0" + searchDate[1];

            if (searchDate[2].length() == 1)
                searchDate[2] = "0" + searchDate[2];
        }

        return searchDate;
    }

    //================================================================================//

    public static void createAlertDialog(String errorMassage, Context context) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle("Error!")
                .setMessage(errorMassage)
                .setCancelable(false)
                .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    //================================================================================//

    public static String getPropertyValue(String propertyName, Context context) {

        Properties properties = new Properties();
        AssetManager assetManager = context.getAssets();

        try {
            InputStream inputStream = assetManager.open(context.getString(R.string.properties_file_name));
            properties.load(inputStream);
        } catch (IOException e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        return properties.getProperty(propertyName);
    }

    public static String convertNumberToString(Number value) {
        Locale locale = Locale.ENGLISH;
//      NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
//      NumberFormat numberFormat = NumberFormat.getNumberInstance(locale);
        NumberFormat numberFormat = NumberFormat.getInstance(locale);
        numberFormat.setMinimumFractionDigits(2); // trailing zeros
        numberFormat.setMaximumFractionDigits(2); // round to 2 digits

        return numberFormat.format(value);
    }
}