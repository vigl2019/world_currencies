package com.example.worldcurrencies;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.worldcurrencies.adapter.CurrencyAdapter;
import com.example.worldcurrencies.model.CurrencyModel;
import com.example.worldcurrencies.utils.Helper;
import com.example.worldcurrencies.utils.IHelper;
import com.example.worldcurrencies.utils.MaskTextWatcher;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CurrencySearchActivity extends AppCompatActivity {

    @BindView(R.id.acs_currency_icon)
    ImageView currencyIcon;
    @BindView(R.id.acs_currency_code)
    TextView currencyCode;
    @BindView(R.id.acs_currency_title)
    TextView currencyTitle;
    @BindView(R.id.acs_currency_search_text)
    EditText currencySearchText;
    @BindView(R.id.acs_currency_search_button)
    Button currencySearchButton;

    @BindView(R.id.acs_currency_list)
    RecyclerView recyclerViewCurrency;

    //================================================================================//

    private Helper helper;
    private CurrencyAdapter currencyAdapter;

    public IHelper iHelper = new IHelper() {
        @Override
        public void getCurrencyModels(List<CurrencyModel> currencyModels) {
            recyclerViewInit(currencyModels);
        }
    };

    //================================================================================//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_search);

        ButterKnife.bind(this);

        final CurrencyModel currencyModel = getIntent().getParcelableExtra("CurrencyModel");

        currencyIcon.setImageResource(currencyModel.getCurrencyIconId());
        currencyCode.setText(currencyModel.getCurrencyCode());
        currencyTitle.setText(currencyModel.getCurrencyTitle());

        //================================================================================//

        Calendar calendar = GregorianCalendar.getInstance();

        String year = String.valueOf(calendar.get(GregorianCalendar.YEAR));

        String month = String.valueOf(calendar.get(GregorianCalendar.MONTH) + 1);
        if(month.length() == 1){
            month = "0" + month;
        }

        String dayOfMonth = String.valueOf(calendar.get(GregorianCalendar.DAY_OF_MONTH));
        if(dayOfMonth.length() == 1){
            dayOfMonth = "0" + dayOfMonth;
        }

        currencySearchText.setText(year + "-" + month + "-" + dayOfMonth);
        currencySearchText.addTextChangedListener(new MaskTextWatcher("####-##-##"));

        //================================================================================//

        helper = new Helper(CurrencySearchActivity.this);
        helper.getCurrencies(iHelper, currencyModel);

        //================================================================================//

        currencySearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String searchDateString = currencySearchText.getText().toString().trim();

                if(!Helper.validateSearchDateField(searchDateString, CurrencySearchActivity.this)) {
                    return;
                }

                String[] searchDate = Helper.getStringDateArrayForSearch(searchDateString);
                helper.getCurrenciesByDate(searchDate[0] + "-" + searchDate[1] + "-" + searchDate[2],
                                            iHelper, currencyModel);
            }
        });
    }

    //================================================================================//

    private void recyclerViewInit(List<CurrencyModel> currencyModels) {

        currencyAdapter = new CurrencyAdapter(currencyModels, true);

        currencyAdapter.setItemClickListener(new CurrencyAdapter.OnItemClickListener() {
            @Override
            public void onClick(CurrencyModel currencyModel) {

                Intent intent = new Intent(CurrencySearchActivity.this, CurrencyInfoActivity.class);
                intent.putExtra("CurrencyModel", currencyModel);
                startActivity(intent);
            }
        });

        currencyAdapter.setLongItemClickListener(new CurrencyAdapter.OnLongItemClickListener() {
            @Override
            public void onLongClick(CurrencyModel currencyModel) {

                Intent intent = new Intent(CurrencySearchActivity.this, CurrencySearchActivity.class);
                intent.putExtra("CurrencyModel", currencyModel);
                startActivity(intent);
            }
        });

        recyclerViewCurrency.setAdapter(currencyAdapter);
    }
}
