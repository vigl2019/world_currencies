package com.example.worldcurrencies.repository;

import com.example.worldcurrencies.api.ApiFactory;
import com.example.worldcurrencies.model.CurrencyRates;

import retrofit2.Call;

public class CurrencyRepository implements ICurrencyRepository {
    @Override
    public Call<CurrencyRates> getData(String access_key, String base, String symbols) {
        return ApiFactory.getCurrencyService().getRates(access_key, base, symbols);
    }

    @Override
    public Call<CurrencyRates> getDataByDate(String date, String access_key, String base, String symbols) {
        return ApiFactory.getCurrencyService().getRatesByDate(date, access_key, base, symbols);
    }
}
