package com.example.worldcurrencies.repository;

import com.example.worldcurrencies.model.CurrencyRates;

import retrofit2.Call;

public interface ICurrencyRepository {
   Call<CurrencyRates> getData(String access_key, String base, String symbols);
   Call<CurrencyRates> getDataByDate(String date, String access_key, String base, String symbols);
}