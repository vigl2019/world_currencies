package com.example.worldcurrencies;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.worldcurrencies.adapter.CurrencyAdapter;
import com.example.worldcurrencies.model.CurrencyModel;
import com.example.worldcurrencies.utils.Helper;
import com.example.worldcurrencies.utils.IHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CurrencyInfoActivity extends AppCompatActivity {

    @BindView(R.id.aci_currency_icon)
    ImageView currencyIcon;
    @BindView(R.id.aci_currency_code)
    TextView currencyCode;
    @BindView(R.id.aci_currency_title)
    TextView currencyTitle;
    @BindView(R.id.aci_currency_description)
    TextView currencyDescription;

    @BindView(R.id.aci_currency_list)
    RecyclerView recyclerViewCurrency;

    //================================================================================//

    private Helper helper;
    private CurrencyAdapter currencyAdapter;

    public IHelper iHelper = new IHelper() {
        @Override
        public void getCurrencyModels(List<CurrencyModel> currencyModels) {
            recyclerViewInit(currencyModels);
        }
    };

    //================================================================================//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_info);

        ButterKnife.bind(this);

        CurrencyModel currencyModel = getIntent().getParcelableExtra("CurrencyModel");

        currencyIcon.setImageResource(currencyModel.getCurrencyIconId());
        currencyCode.setText(currencyModel.getCurrencyCode());
        currencyTitle.setText(currencyModel.getCurrencyTitle());
        currencyDescription.setText(currencyModel.getCurrencyDescription());

        //================================================================================//

        helper = new Helper(CurrencyInfoActivity.this);
        helper.getCurrencies(iHelper, currencyModel);
    }

    //================================================================================//

    private void recyclerViewInit(List<CurrencyModel> currencyModels){

        currencyAdapter = new CurrencyAdapter(currencyModels, true);

        currencyAdapter.setItemClickListener(new CurrencyAdapter.OnItemClickListener() {
            @Override
            public void onClick(CurrencyModel currencyModel) {

                Intent intent = new Intent(CurrencyInfoActivity.this, CurrencyInfoActivity.class);
                intent.putExtra("CurrencyModel", currencyModel);
                startActivity(intent);
            }
        });

        currencyAdapter.setLongItemClickListener(new CurrencyAdapter.OnLongItemClickListener() {
            @Override
            public void onLongClick(CurrencyModel currencyModel) {

                Intent intent = new Intent(CurrencyInfoActivity.this, CurrencySearchActivity.class);
                intent.putExtra("CurrencyModel", currencyModel);
                startActivity(intent);
            }
        });

        recyclerViewCurrency.setAdapter(currencyAdapter);
    }
}

