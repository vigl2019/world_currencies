package com.example.worldcurrencies.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

public class CurrencyModel implements Parcelable {

    private String currencyIconName;
    private int currencyIconId;
    private String currencyTitle;
    private String currencyCode;
    private String currencyDescription;
    private String currencyRate;

    public CurrencyModel(){}

    public CurrencyModel(String currencyIconName, String currencyCode, String currencyTitle,
                         String currencyDescription, String currencyRate) {
        this.currencyIconName = currencyIconName;
        this.currencyCode = currencyCode;
        this.currencyTitle = currencyTitle;
        this.currencyDescription = currencyDescription;
        this.currencyRate = currencyRate;
    }

    //================================================================================//

    protected CurrencyModel(Parcel in) {
        currencyIconName = in.readString();
        currencyIconId = in.readInt();
        currencyTitle = in.readString();
        currencyCode = in.readString();
        currencyDescription = in.readString();
        currencyRate = in.readString();
    }

    public static final Creator<CurrencyModel> CREATOR = new Creator<CurrencyModel>() {
        @Override
        public CurrencyModel createFromParcel(Parcel in) {
            return new CurrencyModel(in);
        }

        @Override
        public CurrencyModel[] newArray(int size) {
            return new CurrencyModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(currencyIconName);
        dest.writeInt(currencyIconId);
        dest.writeString(currencyTitle);
        dest.writeString(currencyCode);
        dest.writeString(currencyDescription);
        dest.writeString(currencyRate);
    }

    //================================================================================//

    public String getCurrencyIconName() {
        return currencyIconName;
    }

    public void setCurrencyIconName(String currencyIconName) {
        this.currencyIconName = currencyIconName;
    }

    public int getCurrencyIconId() {
        return currencyIconId;
    }

    public void setCurrencyIconId(int currencyIconId) {
        this.currencyIconId = currencyIconId;
    }

    public void setCurrencyIconIdByCurrencyCode(Context context){
        int currencyIconId = context.getResources().getIdentifier(this.currencyIconName,
                                                            "drawable", context.getPackageName());
        this.currencyIconId = currencyIconId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyTitle() {
        return currencyTitle;
    }

    public void setCurrencyTitle(String currencyTitle) {
        this.currencyTitle = currencyTitle;
    }

    public String getCurrencyDescription() {
        return currencyDescription;
    }

    public void setCurrencyDescription(String currencyDescription) {
        this.currencyDescription = currencyDescription;
    }

    public String getCurrencyRate() {
        return currencyRate;
    }

    public void setCurrencyRate(String currencyRate) {
        this.currencyRate = currencyRate;
    }
}




